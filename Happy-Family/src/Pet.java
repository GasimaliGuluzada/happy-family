import java.util.Arrays;

public class Pet {
    private  String species;
    private  String nickname;
    private  String[] habits;
    private  int age;
    private  int trickLevel;

    public void eat(){
        System.out.println("I am eating");
    }

    public void respond(){
        System.out.printf("Hello, owner. I am - %s . I miss you! \n",this.nickname);
    }

    public void foul(){
        System.out.println("I need to cover it up");
    }

    public Pet(String nickname) {
        this.nickname = nickname;
    }

    public Pet(){};

    public Pet(String nickname, String species, int age, String[] habits, int trickLevel) {
        this(nickname);
        this.habits = habits;
        this.age = age;
        this.species = species;

        if (0 <= trickLevel && trickLevel <= 100) {
            this.trickLevel = trickLevel;
        }
    }

    @Override
    public String toString() {


        return  '\'' + this.getSpecies() + '{' +
                "nickname='" + this.getName()  + '\'' +
                ", age=" + this.getAge() +
                ", trickLevel='" + this.getTrickLevel() +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }

    public String getName(){
        return this.nickname;
    }

    public String getSpecies(){
        return this.species;
    }

    public int getAge(){
        return this.age;
    }

    public int getTrickLevel(){
        return this.trickLevel;
    }

    public String[] getHabits(){
        return this.habits;
    }
}
