import java.util.Arrays;

public class Human {
   private String name;
   private String surname;
   private int  year;
   private int   IQ_level;
   private Pet   pet;
   private Human mother;
   private Human father;
   private String[][] schedule ;

   public void greetPet(){
       System.out.printf("Hello, %s",pet.getName());
   }


   public void describePet(){
       String TRICK_LEVEL = (pet.getTrickLevel()>50)?"very sly":("almost not sly");
       System.out.print.format("I have an %s is %d years old, he is %s \n", pet.getSpecies(),pet.getAge(),TRICK_LEVEL);
   }

   @Override
   // `Human{name='Michael', surname='Karleone', year=1977, iq=90, mother=Jane Karleone, father=Vito Karleone, pet=dog{nickname='Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}}`
   public String toString() {
      return "'Human{" +
              "name='" + name + '\'' +
              ", surname='" + surname + '\'' +
              ", year=" + year +
              ", IQ_level=" + IQ_level +
              ", pet=" + pet +
              ", mother=" + mother.getName() +
              ", father=" + father.getName() +
              ", pet=" + pet;
   }
/*- constructor which describes the name, surname and the date of birth
    - constructor which describes the name, surname, date of birth, father and mother
    - constructor which describes all the fields
    - empty constructor*/
   public Human() {}

   public Human(String name, String surname, int year) {
      this.name = name;
      this.surname = surname;
      this.year = year;}

   public Human(String name, String surname, int year, Human mother, Human father) {
      this(name,surname,year);
      this.father = father;
      this.mother = mother;
   }



   public Human(String name, String surname, int year,  Human mother, Human father,int IQ_level, Pet pet, String[][] schedule) {
      this(name, surname, year, mother, father);
      this.schedule = schedule;
      this.pet = pet;
      if (0 <= IQ_level && IQ_level <= 100) {this.IQ_level = IQ_level;}
   }

   public String getName() {return name;}

   public String getSurname() {return surname;}

   public Pet getPet() {return pet;}
}
